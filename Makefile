CC=clang
CFLAGS= -std=c99 -g -ggdb -O3
SRC = aes_atmega16.c

all: aes

aes: $(SRC)
	$(CC) -o $@ $^ $(CFLAGS)

aes_debug: $(SRC)
	$(CC) -o $@ $^ $(CFLAGS) -D _DEBUG

aes_verbose: $(SRC)
	$(CC) -o $@ $^ $(CFLAGS) -D _DEBUG -D _VERBOSE

clean:
	rm -f aes aes aes_debug aes_verbose *.o *.out
